from fastapi import FastAPI
from starlette.requests import Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from app.db.sql_worker import get_user_by_token, get_user_by_username, create_new_user, encode, decode
import time

app = FastAPI()
app.mount("/static", StaticFiles(directory="app/static"), name="static")
templates = Jinja2Templates(directory='app/templates')


@app.get("/")
async def login_form(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})


@app.get("/token")
async def login_form_123(request: Request):
    return templates.TemplateResponse("token.html", {"request": request})


@app.get("/auth")
async def login(username: str, password: str):
    if not username or not password:
        return 'Нужно ввести логин и пароль!'
    user = await get_user_by_username(raw_user_name=username)
    if not user:
        return 'Пользователь не зарегистрирован в базе!'
    else:
        user = user.decoded_user()
        print(user)
        if user.password != password:
            return 'Введен неправильный пароль!'
        else:
            if user.token and user.token_exp_date > time.time():
                return 'У вас есть активный токен: {} ' \
                       'Время активности: {} мин:cек'.format(user.token, user.token_alive_time)
            elif user.token and user.token_exp_date <= time.time():
                await user.get_new_token()
                return 'Старый токен больше не действителен! Новый токен: {}'.format(user.token)
            else:
                await user.get_new_token()
                return 'Выдай токен {}'.format(user.token)


@app.get("/check_token")
async def token_worker(token: str):
    if not token:
        return 'Поле не может быть пустым'
    else:
        user = await get_user_by_token(raw_token=token)
        if not user:
            return 'Такого токена не существует!'
        else:
            if user.token_exp_date >= time.time():
                return f'Информация о пользователе {user.username} Зарплата: {user.salary} ' \
                       f'Следующее повышение: {user.next_promotion}'
            return 'Время жизни токена истекло'


@app.get("/new")
async def login_form(request: Request):
    return templates.TemplateResponse("new.html", {"request": request})


@app.get("/create_new_user")
async def login(username: str, password: str, password_confirm: str):
    if not username or not password or not password_confirm:
        return 'Нужно заполнить все поля!'
    else:
        user = await get_user_by_username(raw_user_name=username)
        if user:
            return 'Пользователь с таким именем зарегистрирован в системе'
        elif password != password_confirm:
            return 'Пароли не совпадают!'
        else:
            await create_new_user(username=username, password=password)
            return 'Пользователь {} успешно создан!'.format(username)

@app.get("/edit")
async def login_form(request: Request):
    return templates.TemplateResponse("edit.html", {"request": request})
