function checkauth() {
  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;
  var url = "auth/?username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var result = xhr.responseText;
      document.getElementById("result").textContent = result;
    }
  };
  xhr.send();
}

function checkToken() {
  var token = document.getElementById("token").value;
  var url = "check_token/?token=" + encodeURIComponent(token);
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var result = xhr.responseText;
      document.getElementById("result").textContent = result;
    }
  };
  xhr.send();
}


function new_user() {
  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;
  var password_confirm = document.getElementById("password_confirm").value;
  var url = "create_new_user/?username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password)
  + "&password_confirm=" + encodeURIComponent(password_confirm);
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var result = xhr.responseText;
      document.getElementById("result").textContent = result;
    }
  };
  xhr.send();
}

