import datetime
import time
import aiosqlite
from dataclasses import dataclass
from typing import List, Dict, Union
from pathlib import Path
import loguru
import secrets
import base64
import msgpack


db_path = Path('app/db/main.db')


def convert(seconds: int) -> str:
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return "%02d:%02d" % (minutes, seconds)


@dataclass
class User:
    id: int
    username: str
    password: Union[bytes, str]
    token: Union[None, bytes, str]
    token_exp_date: Union[None, int]
    salary: Union[None, int]
    next_promotion: Union[None, int]


    # def __post_init__(self):
    #     self.username = decode(decoded_str=self.username)
    #     if self.token_exp_date:
    #         if self.token_exp_date >= time.time():
    #             self.token_alive_time = convert(int(self.token_exp_date - time.time()))


    async def get_new_token(self):
        token_alive_time = 15 * 60
        self.token = encode(secrets.token_hex(16))
        self.token_exp_date = int(time.time() + token_alive_time)
        self.token_exp_date = encode(encode_value=self.token_exp_date)
        await self.serialize_data()

    async def serialize_data(self):
        sql = 'UPDATE users SET token=?, token_exp_date=?, salary=?, next_promotion=?, password=? WHERE username=?'
        args = [self.token, self.token_exp_date, self.salary, self.next_promotion, self.password, self.username]
        await sql_execute(sql=sql, args=args)


async def create_new_user(username: str, password: str):
    username = encode(encode_value=username)
    password = encode(encode_value=password)
    sql = f'INSERT INTO users(username, password) VALUES(?,?)'
    await sql_execute(sql=sql, args=[username, password])


def decode(decoded_str):
    encoded_to_bytes = base64.b64decode(decoded_str)
    bytes_to_str = msgpack.loads(encoded_to_bytes)
    return bytes_to_str


def encode(encode_value: Union[str, int, datetime.datetime]):
    str_to_bytes = msgpack.dumps(encode_value)
    return base64.b64encode(str_to_bytes)


async def get_user_by_username(raw_user_name: str) -> Union[User, None]:
    raw_user_name = encode(encode_value=raw_user_name)
    sql = '''SELECT id, username, password, COALESCE(token, '') as token, COALESCE(token_exp_date, 0) as token_exp_date, 
    COALESCE(salary, 0) as salary, COALESCE(next_promotion, '') as next_promotion
    FROM users WHERE username=?'''
    res = await sql_fetchone(sql=sql, args=[raw_user_name])
    if res:
        return User(**res)
    else:
        return None


async def get_user_by_token(raw_token: str)  -> Union[User, None]:
    sql = '''SELECT id, username, password, COALESCE(token, '') as token, COALESCE(token_exp_date, 0) as token_exp_date, 
    COALESCE(salary, 0) as salary, COALESCE(next_promotion, '') as next_promotion
    FROM users WHERE token=?'''
    res = await sql_fetchone(sql=sql, args=[raw_token])
    if res:
        return User(**res)
    else:
        return None


async def sql_execute(sql: str, args: List) -> None:
    async with aiosqlite.connect(db_path) as db:
        try:
            await db.execute('BEGIN')
            await db.execute(sql, args)
            await db.execute('COMMIT')
        except aiosqlite.Error as e:
            await db.execute('ROLLBACK')
            loguru.logger.error(f"Error executing SQL statement '{sql}'")
            loguru.logger.exception(e)
            raise


async def sql_execute_many(sql: str, args: List) -> None:
    async with aiosqlite.connect(db_path) as db:
        try:
            await db.execute('BEGIN')
            await db.executemany(sql, args)
            await db.execute('COMMIT')
        except Exception as e:
            await db.execute('ROLLBACK')
            loguru.logger.error(f'Error executing SQL statement "{sql}"')
            loguru.logger.exception(e)
            raise e


async def sql_fetchone(sql: str, args: List) -> Dict or None:
    async with aiosqlite.connect(db_path) as db:
        try:
            cur = await db.execute(sql, args)
            res = await cur.fetchone()
        except Exception as e:
            loguru.logger.error(f'Error executing SQL statement "{sql}"')
            loguru.logger.exception(e)
            raise e
    if not res:
        return None
    if res:
        columns = [col[0] for col in cur.description]
        return dict(zip(columns, res))


async def sql_fetchall(sql: str, args: List[List]) -> List[Dict] or None:
    async with aiosqlite.connect(db_path) as db:
        try:
            cur = await db.execute(sql, args)
            res = await cur.fetchall()
        except Exception as e:
            loguru.logger.error(f'Error executing SQL statement "{sql}"')
            loguru.logger.exception(e)
            raise e
    if not res:
        return None
    if res:
        columns = [col[0] for col in cur.description]
        return [dict(zip(columns, i)) for i in res]
